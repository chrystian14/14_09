from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.views import APIView, Request, Response, status
from rest_framework_simplejwt.authentication import JWTAuthentication

from .permissions import CustomPermission
from .serializers import RegisterSerializer


class RegisterView(APIView):
    def post(self, request: Request) -> Response:
        serializer = RegisterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status.HTTP_201_CREATED)


class UmaViewQualquerView(APIView):
    # Define qual forma de authenticação para acessar as rotas
    authentication_classes = [JWTAuthentication]
    permission_classes = [CustomPermission]

    def get(self, request: Request):
        return Response({"data": "olá GET"})

    def post(self, request: Request):
        return Response({"data": "olá POST"})
